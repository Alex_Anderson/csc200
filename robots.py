from gasp import *
from random import randint


class Player:
    pass


class Robot:
    pass


begin_graphics()
finished = False
numbots = randint(5,10)

def collided():
    global robots, player
    for robot in robots:
        if abs(int((robot.x - player.x))) < 6 and abs(int((robot.y-player.y))) < 6:
            return True
        else:
            return False


def place_robots():
    global robots
    robots = list()
    for i in range(numbots):
        robot = Robot()
        robot.x = randint(0, 63)
        robot.y = randint(-1, 46)
        robot.shape = Box((10*robot.x+5, 10*robot.y+5), 5, 5)
        robots.append(robot)

def place_player():
    global player
    player = Player()
    player.x = randint(0, 63)
    player.y = randint(-1, 46)

def safely_place_player():
    global player
    place_player()

    # while collided():
    #     place_player()

    player.shape = Circle((10*player.x+5, 10*player.y+5), 5, filled=True)


def move_player():
    global player
    key = update_when('key_pressed')
    while key == '5':
        remove_from_screen(player.shape)
        safely_place_player()
        key = update_when('key_pressed')
    if key == '8' and player.y < 46:
        player.y += 1
    elif key == '6' and player.x < 62:
        player.x += 1
    elif key == '2' and player.y > -1:
        player.y -= 1
    elif key == '4' and player.x > 0:
        player.x -= 1
    elif key == '9':
        if player.x < 63:
            player.x += 1
        if player.y < 46:
            player.y += 1
    elif key == '3':
        if player.x < 63:
            player.x += 1
        if player.y > -1:
            player.y -= 1
    elif key == '1':
        if player.x > 0:
            player.x -= 1
        if player.y > -1:
            player.y -= 1
    elif key == '7':
        if player.x > 0:
            player.x -= 1
        if player.y < 46:
            player.y += 1
    move_to(player.shape, (10*player.x+5, 10*player.y+5))
    sleep(0.02)

def move_robot():
    global robots, player
    for robot in robots:
        if robot.x > player.x and robot.y > player.y:
            robot.x -= 1
            robot.y -= 1
        elif robot.x < player.x and robot.y > player.y:
            robot.x += 1
            robot.y -= 1
        elif robot.x < player.x and robot.y < player.y:
            robot.x += 1
            robot.y += 1
        elif robot.x > player.x and robot.y < player.y:
            robot.x -= 1
            robot.y += 1
        elif robot.x > player.x:
            robot.x -= 1
        elif robot.x < player.x:
            robot.x += 1
        elif robot.y > player.y:
            robot.y -= 1
        elif robot.y < player.y:
            robot.y += 1

        move_to(robot.shape, (10*robot.x+5, 10*robot.y+5))
        sleep(0.02)

def check_collisions():
    global finished
    if collided() == True:
        finished = True
        Text("Game Over", (320, 240), size=48)
        sleep(3)


place_robots()
safely_place_player()

while not finished:
    move_player()
    move_robot()
    check_collisions()
end_graphics()
