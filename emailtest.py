import smtplib

#Establish SMTP Connection
s = smtplib.SMTP('smtp.gmail.com', 587)

#Start TLS based SMTP Session
s.starttls()

#Login Using Your Email ID & Password
s.login("email@gmail.com", "password")


alphabets_in_capital=[]
for i in range(31,127):
    alphabets_in_capital.append(chr(i))
print(alphabets_in_capital)

code = ""

import random

for i in range(0,8):
    code = code + str(random.choice(alphabets_in_capital))

print(code)


#Email Body Content
message = """
Hello, this is a test message!
Illustrated for WTMatter Python Send Email Tutorial
<h1>How are you?</h1>
""" + code

#To Send the Email
s.sendmail("email@gmail.com", "email@gmail.com", message)

#Terminating the SMTP Session
s.quit()
