
#xml

import xml.etree.ElementTree as ETAPI
tree = ETAPI.parse('xmltest.xml')
root = tree.getroot()
#messing around with tutorial stuff
print(root.tag)
print(root.attrib)
#print out go through child
for content in root:
    print(content.tag)
    #go through usernames and find passwords
    #print(usernames)
    for elements in content:
        print(elements.attrib)
        print(elements.tag)
        print(elements.text)
    print("still searching for passwords")
    #failed attempt to gets the node for userdata where passwords are
    users = content.find("userdata")
    print(users)
print("finished sorting through content")
#got the node for userdate where users are
users = (root.find("./userdata"))
print(users)
#print the user elements
users = users.findall("./user")
print(users)
#go through our users and print their attributes (usernames) and the attributes of their password objects
for user in users:
    print(user.attrib)
    password = user.find("./pass")
    print(password.attrib)
print("passwords found!")
