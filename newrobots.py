from gasp import *
from random import randint
from time import sleep


class RobotsGame:
    LEFT_KEY = "KP_4"
    UP_LEFT_KEY = "KP_7"
    UP_KEY = "KP_8"
    UP_RIGHT_KEY = "KP_9"
    RIGHT_KEY = "KP_6"
    DOWN_RIGHT_KEY = "KP_3"
    DOWN_KEY = "KP_2"
    DOWN_LEFT_KEY = "KP_1"
    TELEPORT_KEY = "KP_5"
    RIGHT_EDGE = 63
    LEFT_EDGE = 0
    TOP_EDGE = 47
    BOTTOM_EDGE = 0

    def __init__(self):
        begin_graphics()
        self.finished = False
        self.player = Player()
        self.robots = [Robot() for i in range(5)]
        self.junk = []

    def next_move(self):
        self.player.move()
    #    self.robot.move(self.player)
        for robot in self.robots:
            robot.move(self.player)
        print(str(self.player.x))
        # self.player.check_collosions(self.robot)
    def check_collisions(self):
        #set lists to empty so it remakes the list each return
        junk = []
        #create templist to cache values of robots to refill ater
        templist = self.robots
        self.robots = []
        print(str(templist))
        for robot in templist:
            if robot.x == self.player.x and robot.y == self.player.y:
                self.finished = True
                Text("You've been caught!", (120, 240), size=36)
                sleep(3)
            else:
                for robotn in templist:
                    if robotn != robot and robot.x == robotn.x and robotn.y == robot.y:
                        #ifnotjunk
                        self.junk.append(robot)
                        remove_from_screen(robot.shape)
                        robot.shape = Box((10*robot.x, 10*robot.y), 10, 10, filled=True)
            if len(self.junk) > 0:
                for jbot in self.junk:
                    if robot.x == jbot.x and robot.y == jbot.y:
                    #    self.junk.append(robot)
                        pass
                    else:
                        print("robot added")
                        if (robot in self.robots) == False and (robot in self.junk) == False:
                            self.robots.append(robot)
                        print(self.robots)
            else:
                if (robot in self.robots) == False and (robot in self.junk) == False:
                    self.robots.append(robot)
            for jbot in self.junk:
                if robot.x == jbot.x and robot.y == jbot.y and (robot in self.junk) == False:
                    self.junk.append(robot)
                    remove_from_screen(robot.shape)
                    robot.shape = Box((10*robot.x, 10*robot.y), 10, 10, filled=True)










    def over():
        end_graphics()


class Player:
    def __init__(self):
        self.place()

    def place(self):
        self.x = randint(RobotsGame.LEFT_EDGE, RobotsGame.RIGHT_EDGE)
        self.y = randint(RobotsGame.BOTTOM_EDGE, RobotsGame.TOP_EDGE)
        self.shape = Circle((10 * self.x, 10 * self.y), 5, filled=True)

    def teleport(self):
        remove_from_screen(self.shape)
        self.place()

    def move(self):
        key = update_when("key_pressed")

        if key == '5':
            self.teleport()
            key = update_when("key_pressed")
        player = self
        if key == '8' and player.y < 46:
            player.y += 1
        elif key == '6' and player.x < 62:
            player.x += 1
        elif key == '2' and player.y > -1:
            player.y -= 1
        elif key == '4' and player.x > 0:
            player.x -= 1
        elif key == '9':
            if player.x < 63:
                player.x += 1
            if player.y < 46:
                player.y += 1
        elif key == '3':
            if player.x < 63:
                player.x += 1
            if player.y > -1:
                player.y -= 1
        elif key == '1':
            if player.x > 0:
                player.x -= 1
            if player.y > -1:
                player.y -= 1
        elif key == '7':
            if player.x > 0:
                player.x -= 1
            if player.y < 46:
                player.y += 1

        move_to(self.shape, (10 * self.x, 10 * self.y))


class Robot:
    def __init__(self):
        self.place()

    def place(self):
        self.x = randint(RobotsGame.LEFT_EDGE, RobotsGame.RIGHT_EDGE)
        self.y = randint(RobotsGame.BOTTOM_EDGE, RobotsGame.TOP_EDGE)
        self.shape = Box((10 * self.x, 10 * self.y), 10, 10)

    def move(self, player):
        if self.x < player.x:
            self.x += 1
        elif self.x > player.x:
            self.x -= 1

        if self.y < player.y:
            self.y += 1
        elif self.y > player.y:
            self.y -= 1

        move_to(self.shape, (10 * self.x, 10 * self.y))


game = RobotsGame()

while not game.finished:
    print(str(game.robots))
    game.check_collisions()
    game.next_move()
    if len(game.robots) == 0:
        game.finished = True

game.over()
