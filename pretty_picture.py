from gasp import *

begin_graphics()

#Line((200, 100), (100, 300))
#Circle((300, 200), 100)

#Arc((300, 200), 20, 225, 315)

def draw_face(center):
    #center[0] is the x, center[1] is the y
    #draw head
    Circle((center[0],center[1]), 40)
    #draw eyes
    Circle((center[0]-15,center[1]+10), 5)
    Circle((center[0]+15,center[1]+10), 5)
    Line((center[0], center[1]+10),(center[0]-10,center[1]-10))
    Line((center[0]-10,center[1]-10), (center[0]+10, center[1]-10))
    Arc((center[0],center[1]), 30, 225, 100)

def draw_body(center):
    #center is center of Circle
    Line((center[0], center[1]-40), (center[0], center[1]-140))
    #draw legs
    Line((center[0], center[1]-140),(center[0]-50, center[1]-190))
    Line((center[0], center[1]-140),(center[0]+50, center[1]-190))
    #draw arm
    Line((center[0], center[1]-60),(center[0]-55, center[1]-80))
    Line((center[0], center[1]-60),(center[0]+55, center[1]-80))

def draw_person(headcenter):
    draw_body(headcenter)
    draw_face(headcenter)

draw_person([300,200])


draw_person([100,200])

update_when('key_pressed')

end_graphics()
