from minimock import *


class Tk:
    def __init__(self):
        self.tracker = TraceTracker()

    def protocol(self, *args, **kwargs):
        pass

    def title(self, *args, **kwargs):
        pass

    def resizable(self, *args, **kwargs):
        pass

    def destroy(self, *args, **kwargs):
        pass

    def bind(self, *args, **kwargs):
        pass

    def update_idletasks(self, *args, **kwargs):
        pass

    def after(self, *args, **kwargs):
        pass

    def quit(self, *args, **kwargs):
        pass

    def mainloop(self):
        pass


class Canvas:
    def __init__(self, *args, **kwargs):
        self.args = args
        self.kwargs = kwargs

    def pack(self):
        pass

    def update(self):
        pass

    def delete(self, *args, **kwargs):
        pass

    def create_oval(self, *args, **kwargs):
        pass

    def create_rectangle(self, *args, **kwargs):
        pass

    def create_line(self, *args, **kwargs):
        pass

    def move(self, *args, **kwargs):
        pass
